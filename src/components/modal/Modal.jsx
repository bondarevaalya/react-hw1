import { PureComponent } from 'react';
import './modal.scss';

export class Modal extends PureComponent {
    render () {
      const { title, text, hideFn, id, hasCloseButton, actions } = this.props;
      return (
        <div  className="modal" onClick={() => hideFn(id)}>
          <div className="modal-content" onClick={e => e.stopPropagation()}>
              <h2>{title}</h2>
              {hasCloseButton && <button className="close" onClick={() => hideFn(id)}>&#10761;</button>}
              <p>{text}</p>
              <div>
                {actions?.length > 0 && actions[0]}
                {actions?.length > 1 && actions[1]}
              </div>
            </div>
        </div>
      )
    }
  }